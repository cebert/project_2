package Project2;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by cody on 11/18/16.
 * Updated by Jake on 11/29/16
 */
public class GameManager {

    private int curPoints;
    private int totalPoints;
    private int deckIndex;
    private Card curCard;
    private ObservableList<Card> multChoice;
    private ObservableList<Card> cardList;

    //GameManager assumes the deck has at least one card
    //I changed DeckListView to enforce this, by making 'Play'
    //not selectable unless a deck with at least 1 card is highlighted
    public GameManager(Deck deck){
        totalPoints = deck.getTotalPoints();
        curPoints = 0;
        deckIndex = 0;
        cardList = deck.getCardList();
        FXCollections.shuffle(cardList);
        curCard = cardList.get(0);
        multChoice = generateMultChoice();
    }

    public int calculateScore() {
        return (int)((curPoints * 100.0f) / totalPoints);
    }

    public boolean checkAnswer(String selectedAnswer){
        if(curCard.getAnswer().equals(selectedAnswer)) {
            curPoints += curCard.getPointValue();
            return true;
        }
        return false;
    }

    public Card drawCard(){
        if(deckIndex < cardList.size()-1) {
            curCard = cardList.get(++deckIndex);
            generateMultChoice();
            return curCard;
        }
        return null;
    }

    private ObservableList generateMultChoice(){
        multChoice = FXCollections.observableArrayList();
        Card correctCard = curCard;
        for (int i = 0; i < 4; i++) {
            if(i == 0 && correctCard != null) {
                multChoice.add(correctCard);
            } else {
                int randomAnswer = 0;
                boolean contains = true;
                while(contains ) {
                    randomAnswer = (int)(Math.random() * cardList.size());
                    contains = multChoice.contains(cardList.get(randomAnswer));
                }
                multChoice.add(cardList.get(randomAnswer));
            }
        }
        FXCollections.shuffle(multChoice);
        return multChoice;
    }

    public void setCurPoints(int points){this.curPoints = points;}

    public Card getCurCard() {
        return curCard;
    }

    public ObservableList<Card> getMultChoice() {
        return multChoice;
    }

}
