package Project2;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {

    // different scenes to switch between
    private static Stage primaryStage;
    private static DeckManager dm;
    private static Scene deckListScene;

    private static DeckListView dlv;

    @Override
    public void start(Stage primStage) throws Exception {
        primaryStage = primStage;
        dm = new DeckManager();

        // start up in the deck list view
        dlv = new DeckListView(primaryStage, dm);
        VBox deckListView = dlv.getDeckListView();

        deckListScene = new Scene(deckListView, 450, 400);

        primaryStage.setTitle("FlashApp");
        primaryStage.setScene(deckListScene);
        primaryStage.show();

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                DeckDetailView.checkForUnsavedChanges();
            }
        });
    }

    @Override
    public void stop() {
        dm.save();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static DeckManager getDm() {
        return dm;
    }

    public static void goBack(Deck deck){
        dlv.refreshList();
        deck = dlv.deckList.getItems().get(0);//dlv.deckList.getItems().indexOf(deck)-1);
        dlv.setSelectedDeck(deck);
        primaryStage.setScene(deckListScene);
    }

}
