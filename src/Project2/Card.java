package Project2;

/**
 * Created by cody on 11/11/16.
 * Basic Card object: Meant to be part of a deck
 */
public class Card {

    private String question;
    private String answer;
    private int pointValue;

    public Card(){
        question = null;
        answer = null;
        pointValue = 0;
    }

    public Card(String q, String a, int p){
        question = q;
        answer = a;
        pointValue = p;
    }
    //Getters
    public String getQuestion(){
        return this.question;
    }
    public String getAnswer(){
        return this.answer;
    }
    public int getPointValue() {
        return pointValue;
    }

    //Setters
    public void setPointValue(int pointValue) {
        this.pointValue = pointValue;
    }
    public void setQuestion(String question) {
        this.question = question;
    }
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String toString(){
        return question;
    }
}
