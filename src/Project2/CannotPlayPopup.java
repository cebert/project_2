package Project2;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Button;

/**
 * Created by Jake on 11/29/16.
 */
public class CannotPlayPopup {
    private Stage cannotPlayPop = new Stage();
    private VBox popupScene = new VBox(15);
    private Button okButton = new Button("Ok");
    Label message = new Label("You must select a Deck that has at least 4 cards!");
    public CannotPlayPopup() {
        popupScene.setPadding(new Insets(15));
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                cannotPlayPop.close();
            }
        });
        popupScene.getChildren().addAll(message, okButton);
        popupScene.setAlignment(Pos.CENTER);
        cannotPlayPop.setScene(new Scene(popupScene));
        cannotPlayPop.showAndWait();
    }
}
