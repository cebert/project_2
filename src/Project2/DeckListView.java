package Project2;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.*;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Created by Natalie on 11/15/2016.
 */
public class DeckListView {

    private VBox vbox = new VBox(10);
    private Deck selectedDeck = null;
    private Stage primaryStage;
    private DeckManager deckManager;

    ListView<Deck> deckList;

    private Button deckEdit;
    private Button deleteDeck;

    public DeckListView(Stage primaryStage, DeckManager deckManager){
        this.deckManager = deckManager;
        this.primaryStage = primaryStage;
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(10,0,10,0));

        Text title = new Text("Choose your deck:");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        vbox.getChildren().add(title);

        // buttons
        deckEdit = new Button("Edit");
        deleteDeck = new Button("Delete");

        // list of decks
        deckList = new ListView<>();

        deckList.setItems(deckManager.getDeckList());
        deckList.setMaxWidth(300);

        deckList.setMaxHeight(350);

        if(deckList.getItems().size() > 0) {
            selectedDeck = deckList.getItems().get(0);
            deckEdit.setDisable(false);
            deleteDeck.setDisable(false);
        }

        deckList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                selectedDeck = deckList.getSelectionModel().getSelectedItem();
                deckEdit.setDisable(deckManager.isEmpty());
                deleteDeck.setDisable(deckManager.isEmpty());
            }
        });

        deckEdit.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                goToDeckDetailView();
            }
        });

        if(deckList.getItems().size() == 0)
            deckEdit.setDisable(true);

        Button createDeck = new Button("Create");
        createDeck.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Stage createPopup = createPopup(deckList);
                createPopup.show();
            }
        });

        deleteDeck.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                DeletePopUp popup = new DeletePopUp();
                boolean confirm = popup.confirmDelete();

                if (confirm) {
                    int index = deckList.getItems().indexOf(selectedDeck);
                    deckManager.removeDeck(getSelectedDeck());
                    deckList.getItems().remove(getSelectedDeck());
                    if (deckList.getItems().size() > 0)
                        if(index == 0) {
                            setSelectedDeck(deckList.getItems().get(0));
                        } else {
                            setSelectedDeck(deckList.getItems().get(index - 1));
                        }
                    else {
                        deleteDeck.setDisable(true);
                        deckEdit.setDisable(true);
                    }
                }
            }
        });

        if(deckList.getItems().size() == 0) {
            deleteDeck.setDisable(true);
        }

        Button playButton = new Button("Play");
        playButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(selectedDeck.getCardCount() < 4) {
                    CannotPlayPopup cpp = new CannotPlayPopup();
                } else {
                    GameUI game = new GameUI(selectedDeck);
                    Scene gameScene = new Scene(game.getGameUI(), 650, 420);
                    primaryStage.setScene(gameScene);
                }
            }
        });

        HBox hbox = new HBox(15, createDeck, deckEdit, deleteDeck, playButton);
        hbox.setAlignment(Pos.CENTER);

        vbox.getChildren().add(deckList);
        vbox.getChildren().add(hbox);

    }

    public Stage createPopup(ListView deckList) {
        Stage createPopup = new Stage();
        createPopup.initModality(Modality.APPLICATION_MODAL);
        createPopup.initOwner(primaryStage);
        createPopup.setTitle("Create Deck");

        Text deckName = new Text("Name:");
        TextField tf = new TextField();

        Button save = new Button("Save");
        save.setDefaultButton(true);
        save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String newDeckName = tf.getText();

                boolean uniqueName = deckManager.uniqueName(newDeckName);

                if(newDeckName.isEmpty() || !uniqueName){
                    Stage popupStage = new Stage();
                    popupStage.initModality(Modality.APPLICATION_MODAL);
                    popupStage.setTitle("Invalid Entry");

                    BorderPane scene = new BorderPane();
                    scene.setPadding(new Insets(10));

                    Text badData = new Text("You must enter a unique name for the new deck.");
                    scene.setCenter(badData);
                    popupStage.setScene(new Scene(scene, 320, 100));
                    popupStage.showAndWait();
                }
                else {
                    Deck newDeck = new Deck(newDeckName);
                    selectedDeck = newDeck;
                    deckManager.addDeck(newDeck);
                    createPopup.close();
                    goToDeckDetailView();
                }
            }
        });

        HBox hBox = new HBox(10, deckName, tf, save);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(15));

        Scene popupScene = new Scene(hBox);
        createPopup.setScene(popupScene);
        return createPopup;
    }

    public void goToDeckDetailView() {
        if(getSelectedDeck() != null) {
            DeckDetailView ddv = new DeckDetailView(selectedDeck);
            VBox deckDetailView = ddv.getDeckDetailView();

            Scene deckDetailScene = new Scene(deckDetailView, 650, 400);
            primaryStage.setScene(deckDetailScene);
        }
    }

    public VBox getDeckListView() {
        return vbox;
    }

    public Deck getSelectedDeck(){
        return selectedDeck;
    }

    public void setSelectedDeck(Deck deck){
        selectedDeck = deck;
    }

    public void refreshList(){
        deckList.setItems(null);
        deckList.setItems(deckManager.getDeckList());
    }

}
