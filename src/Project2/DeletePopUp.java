package Project2;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by Natalie on 11/28/2016.
 */

public class DeletePopUp {

    private Stage deletePop = new Stage();
    private VBox popupScene = new VBox(15);
    private HBox buttons = new HBox(10);

    private Button delete;
    private Button cancel;

    private boolean returnValue;

    public DeletePopUp(){

        popupScene.setPadding(new Insets(15));
        buttons.setAlignment(Pos.CENTER);

        Label confirm = new Label("Are you sure you want to delete this?");
        confirm.setAlignment(Pos.CENTER);

        returnValue = false;

        delete = new Button("Delete");
        cancel = new Button("Cancel");

        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                returnValue = true;
                deletePop.close();
            }
        });

        cancel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                returnValue = false;
                deletePop.close();
            }
        });

        buttons.getChildren().addAll(delete, cancel);
        popupScene.getChildren().addAll(confirm,buttons);
        deletePop.setScene(new Scene(popupScene));
        deletePop.showAndWait();
    }

    public boolean confirmDelete(){
        return returnValue;
    }
}
