package Project2;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * Created by Natalie on 11/11/2016.
 *
 * This is the DeckManager class used to control an arraylist of all the decks.
 */
public class DeckManager {

    private ArrayList<Deck> arrayDeckList = new ArrayList<Deck>();
    private ArrayList<Deck> removeList = new ArrayList<Deck>();
    private ObservableList<Deck> deckList = FXCollections.observableList(arrayDeckList);
    private File decksFile;
    private Deck currentDeck;

    /* This construtor creates/finds the decks.txt file, and creates the decks stored
    * in the file. Essentially, this constructor loads the data in the files into the
    * current instance of the program. */
    public DeckManager() {
        try {

            decksFile = new File("decks.txt");
            if (!decksFile.exists()) {
                decksFile.createNewFile();
            }

            Scanner reader = new Scanner(decksFile);
            while(reader.hasNextLine()) {
                String deckName = reader.nextLine();
                Deck deck = new Deck(deckName);
                deckList.add(deck);

                File deckFile = new File(deckName + ".txt");
                if(deckFile.exists()) {
                    Scanner deckReader = new Scanner(deckFile);
                    while (deckReader.hasNextLine()) {
                        String[] line = deckReader.nextLine().split(", ");
                        Card card = new Card();
                        card.setQuestion(line[0]);
                        card.setAnswer(line[1]);
                        card.setPointValue(Integer.parseInt(line[2]));
                        deck.addCard(card);
                    }
                } else {
                    deckFile.createNewFile();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean uniqueName(String name){
        boolean uniqueName = true;
        for(int i = 0; i < deckList.size(); i++) {
            if(deckList.get(i).getDeckName().equalsIgnoreCase(name))
                uniqueName = false;
        }
        return uniqueName;
    }

    public void addDeck(Deck deck){
        boolean uniqueName = uniqueName(deck.getDeckName());
        if(!deckList.contains(deck) && uniqueName)
            deckList.add(deck);
    }

    public void removeDeck(Deck deck){
        if(deckList.removeAll(deck))
            removeList.add(deck);
    }

    public void removeOldDeckName(String name){
        Deck tempDeck = new Deck(name);
        removeList.add(tempDeck);
    }

    //This method takes the data stored in deckList, including card data from each deck,
    //and writes it to the files right before the application exits. It also removes any
    //files of decks that were deleted during the application's session.
    public void save() {
        try {
            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(decksFile)));

            for(int i = 0; i < deckList.size(); i++) {
                Deck deck = deckList.get(i);
                pw.println(deck.getDeckName());

                File newFile = new File(deck.getDeckName() + ".txt");
                PrintWriter cardWriter = new PrintWriter(new BufferedWriter(new FileWriter(newFile)));
                for(int j = 0; j < deck.getCardCount(); j++) {
                    Card card = deck.getCardList().get(j);
                    cardWriter.println(card.getQuestion() + ", " + card.getAnswer() + ", " + card.getPointValue());
                }

                cardWriter.close();
            }

            pw.close();

            for(int i = 0; i < removeList.size(); i++) {
                File removeFile = new File(removeList.get(i).getDeckName() + ".txt");
                if(removeFile.exists())
                    removeFile.delete();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ObservableList getDeckList() {
        return deckList;
    }

    public boolean isEmpty(){
        return deckList.isEmpty();
    }

}
