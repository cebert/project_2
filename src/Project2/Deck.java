package Project2;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;

/**
 * Created by cody on 11/11/16.
 * Modified by Jake on 11/15/16
 */
public class Deck {

    private String deckName = null;
    private ObservableList<Card> cardList;
    private int cardCount;
    private int totalPoints;

    public Deck(String newName) {
        this.deckName = newName;
        cardList = FXCollections.observableArrayList();
        cardCount = 0;
        totalPoints = 0;
    }

    public void addCard(Card newCard) {
        cardList.add(newCard);
        cardCount = cardList.size();
        totalPoints += newCard.getPointValue();
    }

    public void removeCard(Card deleteCard){
        if(!isEmpty()) {
            int index = cardList.indexOf(deleteCard);
            totalPoints -= deleteCard.getPointValue();
            cardList.remove(index);
            cardCount = cardList.size();
        }
    }

    public String getDeckName() {
        return deckName;
    }

    public void setDeckName(String deckName) {
        this.deckName = deckName;
    }

    public int getCardCount() {
        return cardCount;
    }


    public ObservableList<Card> getCardList() {
        return cardList;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void adjustTotalPoints(Card card, int newPoints) {
        if(cardList.contains(card)) {
            int diff = newPoints - card.getPointValue();
            totalPoints += diff;
        }
    }

    public boolean isEmpty(){
        return cardList.isEmpty();
    }

    public String toString(){
        return this.deckName;
    }

}
