package Project2;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by Natalie on 11/16/2016.
 */
public class CardEditView {

    private Stage popupStage;
    private BorderPane popupScene;
    private GridPane newCard;
    private GridPane editCard;

    private int sceneWidth = 275;
    private int sceneHeight = 200;

    // labels and text fields
    private Label ques;
    private Label ans;
    private Label points;

    private TextField quesText;
    private TextField ansText;
    private TextField ptsText;

    // save and cancel buttons
    private Button save;
    private Button cancel;

    private Deck deck;
    private Card card;

    public CardEditView(Deck d, Card c){

        deck = d;
        card = c;
        popupStage = new Stage();
        popupStage.initModality(Modality.APPLICATION_MODAL);
        popupScene = new BorderPane();

        ques = new Label("Question/Term");
        ans = new Label("Answer/Definition");
        points = new Label("# of Points");

        quesText = new TextField();
        ansText = new TextField();
        ptsText = new TextField("0");

        save = new Button("Save");
        cancel = new Button("Cancel");

        save.setDefaultButton(true);
        save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                save();
            }
        });

        cancel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                cancel();
            }
        });

        HBox buttons = new HBox(10);
        buttons.setAlignment(Pos.CENTER);
        buttons.setPadding(new Insets(0,0,10,0));
        buttons.getChildren().addAll(save, cancel);

        popupScene.setBottom(buttons);
        popupStage.setScene(new Scene(popupScene, sceneWidth, sceneHeight));
    }

    public void showNewCard(){
        newCard = new GridPane();
        newCard.setHgap(10);
        newCard.setVgap(10);
        newCard.setPadding(new Insets(0, 10, 0, 10));

        newCard.add(ques, 0, 1);
        newCard.add(quesText, 1, 1);
        newCard.add(ans, 0, 2);
        newCard.add(ansText, 1, 2);
        newCard.add(points, 0, 3);
        newCard.add(ptsText, 1, 3);

        popupScene.setCenter(newCard);
        popupStage.showAndWait();
    }

    public void showEditCard(Card card){
        editCard = new GridPane();
        editCard.setHgap(10);
        editCard.setVgap(10);
        editCard.setPadding(new Insets(0, 10, 0, 10));

        quesText.setText(card.getQuestion());
        ansText.setText(card.getAnswer());
        ptsText.setText("" + card.getPointValue());

        editCard.add(ques, 0, 1);
        editCard.add(quesText, 1, 1);
        editCard.add(ans, 0, 2);
        editCard.add(ansText, 1, 2);
        editCard.add(points, 0, 3);
        editCard.add(ptsText, 1, 3);

        popupScene.setCenter(editCard);
        popupStage.showAndWait();
    }

    public void save(){

        boolean qIsEmpty = quesText.getText().isEmpty();
        boolean aIsEmpty = ansText.getText().isEmpty();
        boolean pIsEmpty = ptsText.getText().isEmpty();

        if(qIsEmpty || aIsEmpty || pIsEmpty){
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);
            popupStage.setTitle("Invalid Entry");

            BorderPane scene = new BorderPane();
            scene.setPadding(new Insets(10));

            Text badData = new Text("You must enter both a question and an answer");
            scene.setCenter(badData);
            popupStage.setScene(new Scene(scene, 300, 100));
            popupStage.showAndWait();
        }

        else {
            String q = quesText.getText();
            String a = ansText.getText();
            int points = Integer.parseInt(ptsText.getText());

            if(card == null) {
                Card cardToAdd = new Card();
                cardToAdd.setQuestion(q);
                cardToAdd.setAnswer(a);
                cardToAdd.setPointValue(points);
                deck.addCard(cardToAdd);
            } else {
                card.setQuestion(q);
                card.setAnswer(a);
                deck.adjustTotalPoints(card, points);
                card.setPointValue(points);
            }

            popupStage.close();
        }
    }

    public boolean cancel(){

        popupStage.close();
        return false;
    }

}
