package Project2;

/**
 * Created by Jake on 11/29/16.
 */
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class GameUI {

    private GameManager gm;
    private BorderPane bp;
    private ToggleGroup buttonGroup;
    private RadioButton answer1;
    private RadioButton answer2;
    private RadioButton answer3;
    private RadioButton answer4;
    private Text question;
    private int curScore = 0;
    private Text answerCheck;

    public GameUI(Deck deck) {
        gm = new GameManager(deck);
        bp = new BorderPane();
        bp.setPadding(new Insets(20));

        Label score = new Label("Score: 0/" + deck.getTotalPoints());
        score.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        BorderPane topPane = new BorderPane();
        topPane.setRight(score);
        bp.setTop(topPane);

        StackPane card = new StackPane();
        Rectangle cardRec = new Rectangle(380, 220);
        cardRec.setFill(Color.WHITE);
        cardRec.setStroke(Color.BLACK);
        question = new Text(gm.getCurCard().getQuestion());
        question.setTextAlignment(TextAlignment.CENTER);
        question.setFont(Font.font("Times", FontWeight.NORMAL, 20));
        question.setWrappingWidth(300);
        card.getChildren().addAll(cardRec, question);

        buttonGroup = new ToggleGroup();

        answer1 = new RadioButton();
        answer1.setToggleGroup(buttonGroup);
        answer1.setPadding(new Insets(0,0,0,45));

        answer2 = new RadioButton();
        answer2.setToggleGroup(buttonGroup);
        answer2.setPadding(new Insets(0,0,0,45));

        answer3 = new RadioButton();
        answer3.setToggleGroup(buttonGroup);
        answer3.setPadding(new Insets(0,0,0,45));

        answer4 = new RadioButton();
        answer4.setToggleGroup(buttonGroup);
        answer4.setPadding(new Insets(0,0,0,45));

        answerCheck = new Text();
        answerCheck.setText("Correct!");
        answerCheck.setFill(Color.GREEN);
        answerCheck.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        answerCheck.setVisible(false);
        topPane.setCenter(answerCheck);
        Label invisibleLabel = new Label("Invisible");
        invisibleLabel.setVisible(false);
        invisibleLabel.setFont(Font.font("Arial", FontWeight.BOLD, 25));
        topPane.setLeft(invisibleLabel);

        buttonGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if(buttonGroup.getSelectedToggle() != null){
                    answer1.setDisable(true);
                    answer2.setDisable(true);
                    answer3.setDisable(true);
                    answer4.setDisable(true);
                    if(buttonGroup.getSelectedToggle().getUserData().equals(gm.getCurCard().getAnswer())){
                        curScore = curScore + gm.getCurCard().getPointValue();
                        gm.setCurPoints(curScore);
                        score.setText("Score: " + curScore + "/" + deck.getTotalPoints());
                        answerCheck.setText("Correct!");
                        answerCheck.setFill(Color.GREEN);
                        answerCheck.setVisible(true);
                    }
                    else if(buttonGroup.getSelectedToggle().getUserData().toString() != gm.getCurCard().getAnswer()){
                        answerCheck.setText("Incorrect");
                        answerCheck.setFill(Color.RED);
                        answerCheck.setVisible(true);
                    }
                    question.setText(gm.getCurCard().getAnswer());
                }
            }
        });


        VBox buttonsCard = new VBox();
        buttonsCard.setPadding(new Insets(10, 50, 0, 50));
        buttonsCard.setSpacing(5);

        buttonsCard.getChildren().addAll(card, answer1, answer2, answer3, answer4);
        bp.setCenter(buttonsCard);

        Button quitButton = new Button("Quit");
        quitButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Main.goBack(deck);
            }
        });
        BorderPane bottomPane = new BorderPane();
        bottomPane.setLeft(quitButton);

        Button nextButton = new Button("Next");
        nextButton.setDefaultButton(true);
        nextButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(gm.drawCard() != null) {
                    updateText();
                } else {
                    Text done = new Text("Done! Score: " + gm.calculateScore() + "%");
                    bottomPane.setCenter(done);
                }
            }
        });

        bottomPane.setRight(nextButton);
        bp.setBottom(bottomPane);
        updateText();
    }

    public BorderPane getGameUI() {
        return bp;
    }

    public void updateText() {
        question.setText(gm.getCurCard().getQuestion());
        answer1.setUserData(gm.getMultChoice().get(0).getAnswer());
        answer1.setText(gm.getMultChoice().get(0).getAnswer());
        answer1.setDisable(false);
        answer1.setSelected(false);
        answer1.setToggleGroup(buttonGroup);
        answer2.setUserData(gm.getMultChoice().get(1).getAnswer());
        answer2.setText(gm.getMultChoice().get(1).getAnswer());
        answer2.setDisable(false);
        answer2.setSelected(false);
        answer2.setToggleGroup(buttonGroup);
        answer3.setUserData(gm.getMultChoice().get(2).getAnswer());
        answer3.setText(gm.getMultChoice().get(2).getAnswer());
        answer3.setDisable(false);
        answer3.setSelected(false);
        answer3.setToggleGroup(buttonGroup);
        answer4.setUserData(gm.getMultChoice().get(3).getAnswer());
        answer4.setText(gm.getMultChoice().get(3).getAnswer());
        answer4.setDisable(false);
        answer4.setSelected(false);
        answer4.setToggleGroup(buttonGroup);
        answerCheck.setVisible(false);
    }
}
