package Project2;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;

/**
 * Created by Natalie on 11/15/2016.
 *
 * This is the UI class for the deck detail view.
 * It should show the deck title and then list the cards in the form of a table
 */
public class DeckDetailView {

    private VBox vbox = new VBox(10);
    private TableView<Card> table = new TableView();
    private int tableWidth = 500;
    private static Deck deck;
    private boolean isEmpty;

    private Button addCard;
    private Button edit;
    private Button deleteCard;
    private Button back;

    private static TextField titleField;

    public DeckDetailView(Deck selectedDeck){

        this.deck = selectedDeck;
        isEmpty = deck.isEmpty();


        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(10,0,10,0));

        titleField = new TextField(deck.getDeckName());
        titleField.setEditable(true);
        titleField.setMaxWidth(200);
        titleField.setAlignment(Pos.CENTER);
        titleField.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        vbox.getChildren().add(titleField);

        // cards in table should start out not editable
        table.setEditable(false);
        table.setMaxWidth(tableWidth);
        TableColumn<Card, String> qCol = new TableColumn("Question/Term");
        TableColumn<Card, String> aCol = new TableColumn("Answer/Definition");
        TableColumn<Card, String> pCol = new TableColumn("Points");

        //set the sizes to be 1/3 and 2/3 of the table list, and make fixed.
        qCol.setPrefWidth(tableWidth/3);
        qCol.setResizable(false);
        aCol.setPrefWidth(tableWidth/2);
        aCol.setResizable(false);
        pCol.setPrefWidth(tableWidth/6);
        pCol.setResizable(false);

        table.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                isEmpty = deck.isEmpty();
                deleteCard.setDisable(isEmpty);
                edit.setDisable(isEmpty);
            }
        });

        // fill table columns with card terms/definitions
        qCol.setCellValueFactory(
                new PropertyValueFactory<Card,String>("question")
        );
        aCol.setCellValueFactory(
                new PropertyValueFactory<Card,String>("answer")
        );
        pCol.setCellValueFactory(
                new PropertyValueFactory<Card,String>("pointValue")
        );

        // buttons to go to add/edit/delete views/popups
        edit = new Button("Edit");
        edit.setAlignment(Pos.CENTER);
        edit.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                // ADD CODE HERE TO BRING UP EDIT CARD WINDOW
                Card thisCard = table.getSelectionModel().getSelectedItem();
                if(thisCard != null) {
                    CardEditView editView = new CardEditView(deck, thisCard);
                    editView.showEditCard(thisCard);
                    refreshTable();
                }
            }
        });

        deleteCard = new Button("Delete");
        deleteCard.setAlignment(Pos.CENTER);
        deleteCard.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                DeletePopUp popup = new DeletePopUp();
                boolean confirm = popup.confirmDelete();

                if(confirm) {
                    Card card = table.getSelectionModel().getSelectedItem();
                    deck.removeCard(card);

                    isEmpty = deck.isEmpty();
                    deleteCard.setDisable(isEmpty);
                    edit.setDisable(isEmpty);
                }
            }
        });

        addCard = new Button("Create");
        addCard.setAlignment(Pos.CENTER);
        addCard.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //ADD CODE HERE TO CREATE NEW CARD CREATION POPUP
                CardEditView editView = new CardEditView(deck, null);
                editView.showNewCard();
            }
        });


        edit.setDisable(true);
        deleteCard.setDisable(true);

        HBox buttons = new HBox(10);
        buttons.getChildren().addAll(addCard, edit, deleteCard);
        buttons.setAlignment(Pos.CENTER);

        // add the deck's cards to the table
        table.setItems(deck.getCardList());
        table.getColumns().addAll(qCol, aCol, pCol);

        // back button to return to deck list/ main menu
        back = new Button("Back to Main Menu");
        back.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(titleField != null){
                    confirmNameChange();
                }
                Main.goBack(selectedDeck);
            }
        });

        // add the table and the edit button to the vbox scene
        vbox.getChildren().add(table);
        vbox.getChildren().add(buttons);
        vbox.getChildren().add(back);

    }

    public VBox getDeckDetailView(){
        return vbox;
    }

    public void refreshTable(){
        table.getColumns().get(0).setVisible(false);
        table.getColumns().get(0).setVisible(true);
    }

    // Method called when the deck name has changed and user clicks back or exits application
    // asks to save or discard the change
    // returns to main menu regardles off save/discard
    public static void confirmNameChange(){

        String title = titleField.getText();
        if(!title.equals(deck.getDeckName())){

            // if going back and a change is detected, ask to confirm change to deck name
            Stage popupStage = new Stage();
            popupStage.initModality(Modality.APPLICATION_MODAL);

            BorderPane scene = new BorderPane();
            scene.setPadding(new Insets(10));

            Text saveChanges = new Text("You've made a change to the name of your deck.\n" +
                    "Do you wish to save this change?");
            scene.setCenter(saveChanges);

            HBox buttons = new HBox(10);
            Button save = new Button("Save");
            save.setDefaultButton(true);
            Button discard = new Button("Discard");

            save.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    Main.getDm().removeOldDeckName(deck.getDeckName());
                    deck.setDeckName(title);
                    popupStage.close();
                }
            });

            discard.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    titleField.setText(deck.getDeckName());
                    popupStage.close();
                }
            });

            buttons.getChildren().addAll(save,discard);
            buttons.setAlignment(Pos.CENTER);
            scene.setBottom(buttons);

            popupStage.setScene(new Scene(scene, 300, 100));
            popupStage.showAndWait();
        }
    }

    // called when the application
    public static boolean checkForUnsavedChanges(){
        if(titleField != null){
            if(!deck.getDeckName().equals(titleField.getText())){
                confirmNameChange();
                return true;
            }
        }
        return true;
    }

}
